﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class PipeSwitcher : MonoBehaviour
{

    public bool active = false;
    public RenderPipelineAsset b;
    public Button yourButton;
    public TextMeshProUGUI infoText;

    // Start is called before the first frame update
    void Start()
    {
        Button btn = yourButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }

    void TaskOnClick()
    {
        active = !active;
        if (active)
        {
            infoText.text = "pipeline: HDR";
            GraphicsSettings.renderPipelineAsset = b;
        } else
        {
            infoText.text = "pipeline: null";
            GraphicsSettings.renderPipelineAsset = null;
        }
    }

}

   
